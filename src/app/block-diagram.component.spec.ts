import { TestBed, async } from '@angular/core/testing';
import { BlockDiagramComponent } from './block-diagram.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BlockDiagramComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(BlockDiagramComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'datatable-latest'`, () => {
    const fixture = TestBed.createComponent(BlockDiagramComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('datatable-latest');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(BlockDiagramComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('datatable-latest app is running!');
  });
});

