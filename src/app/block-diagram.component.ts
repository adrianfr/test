import { Component, ViewChild, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { AppService } from './app.service';
import { TranslateService } from '@ngx-translate/core';
import { delay } from 'rxjs/operators';
import { ViewEncapsulation } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './block-diagram.component.html',
  styleUrls: ['./block-diagram.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BlockDiagramComponent {
  title = 'datatable';
  @ViewChild('myTable', { static: false }) table;
  editing = {};
  form: FormGroup;
  text: string;
  timeout: any;
  temp: any;
  showAscii: boolean;
  source_filter_text: string;
  dest_filter_text: string;
  hasError: boolean;
  rowsData = [];
  start_date: any;
  rows_count: number = 10;
  end_date: any;
  temp2: any;
  columns = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  hasData: boolean;
  loadingState: boolean;
  rowsPerPage: number = 10;
  totalRecords: number;
  errorMessage: any;
  indexOfFirstRecord: any;
  indexOfNewPage: any;
  haveError = false;
  globalFilter: string;
  lastColumnTitle = 'Hex';
  constructor(private http: HttpClient, private app_service: AppService, private translate: TranslateService) { }

  ngOnInit() {
    this.form = new FormGroup({
      'search': new FormControl(null)
    });
    this.hasData = true;
    this.translate.addLangs(['en', 'fr']);
    this.translate.setDefaultLang('en');
    this.translate.use('en');
    setTimeout(() => { this.loadingIndicator = false; }, 500);
    this.getJsonData();
    this.columns = [{ name: 'ts' }, { name: 's', filtering: { filterstring: '', placeholder: 'enter' } }, { name: 'd' }];
  }

  /**to filter the records */
  public filter(data): void {
    this.globalFilter = data;
    localStorage.setItem('filter_text', data);
    const temp = this.temp.filter((d: any) => {
      if (d.s.toLowerCase().includes(data.toLowerCase())) {
        return true;
      }
      if (d.d.toLowerCase().includes(data.toLowerCase())) {
        return true;
      }
      if (d.o.toLowerCase().includes(data.toLowerCase())) {
        return true;
      }
      if (d.ts.toLowerCase().includes(data.toLowerCase())) {
        return true;
      }
      if (d.rule.toLowerCase().includes(data.toLowerCase())) {
        return true;
      }
      if (d.hex.toLowerCase().includes(data.toLowerCase())) {
        return true;
      }
      return false;
    });
    this.totalRecords = temp.length;
    this.rowsData = temp.slice(this.indexOfFirstRecord || 0, this.indexOfFirstRecord + this.rowsPerPage || 10);
  }


  /**source column filter */
  public sourceFilter(): void {
    localStorage.setItem('source_filter_text', this.source_filter_text);
    const temp = this.temp.filter((d: any) => {
      if (d.s.toLowerCase().includes(this.source_filter_text.toLowerCase())) {
        return true;
      }
      return false;
    });
    this.totalRecords = temp.length;
    this.rowsData = temp.slice(this.indexOfFirstRecord || 0, this.indexOfFirstRecord + this.rowsPerPage || 10);
  }

  /**
   * destination column filter
   */
  public destFilter(): void {
    localStorage.setItem('dest_filter_text', this.dest_filter_text);
    const temp = this.temp.filter((d: any) => {
      if (d.d.toLowerCase().includes(this.dest_filter_text.toLowerCase())) {
        return true;
      }
      return false;
    });
    this.totalRecords = temp.length;
    this.rowsData = temp.slice(this.indexOfFirstRecord || 0, this.indexOfFirstRecord + this.rowsPerPage || 10);
  }

  /**assign ascii part instead of hex values */
  public convert() {
    this.lastColumnTitle = this.lastColumnTitle === 'Hex' ? 'Ascii' : 'Hex';
    this.showAscii = !this.showAscii;
    let filterData = this.temp2.filter(x => {
      if (this.source_filter_text) {
        return (x.s.toLowerCase().includes(this.source_filter_text.toLowerCase()));
      }
      if (this.dest_filter_text) {
        return (x.d.toLowerCase().includes(this.dest_filter_text.toLowerCase()));
      }
      if (this.globalFilter) {
        return (x.s.toLowerCase().includes(this.globalFilter.toLowerCase())) ||
          (x.d.toLowerCase().includes(this.globalFilter.toLowerCase())) ||
          (x.o.toLowerCase().includes(this.globalFilter.toLowerCase())) ||
          (x.rule.toLowerCase().includes(this.globalFilter.toLowerCase()));
      } else {
        return x;
      }
    });
    if (this.showAscii) {
      this.rowsData = filterData.slice(this.indexOfFirstRecord || 0, this.indexOfFirstRecord + this.rowsPerPage || 10).map((row) => {
        let hex = '';
        row.data.map((a) => {
          hex += (a.offset) + (a.ascii);
        });
        return {
          ts: row['ts'],
          s: row['s'],
          d: row['d'],
          o: row['o'],
          rule: row['rule'],
          hex: hex
        };
      });
    } else {
      this.rowsData = filterData.slice(this.indexOfFirstRecord || 0, this.indexOfFirstRecord + this.rowsPerPage || 10).map((row) => {
        let hex = '';
        row.data.map((a) => {
          hex += (a.offset) + (a.hex);
        });
        return {
          ts: row['ts'],
          s: row['s'],
          d: row['d'],
          o: row['o'],
          rule: row['rule'],
          hex: hex
        };
      });
    }
  }

  /**to filter the records based on date */
  public applyDateFilter(): void {
    let start_date = new Date(this.start_date).getTime() / 1000;
    let end_date = new Date(this.end_date).getTime() / 1000;
    if (start_date && end_date) {
      const temp = this.temp.filter((row) => {
        if (row.ts >= start_date && row.ts <= end_date) {
          return true;
        }
        return false;
      });
      this.rowsData = temp.slice(this.indexOfFirstRecord || 0, this.indexOfFirstRecord + this.rowsPerPage || 10);
    } else {
      this.rowsData = this.temp.slice(this.indexOfFirstRecord || 0, this.indexOfFirstRecord + this.rowsPerPage || 10);
    }
  }

  /**to download records as csv */
  public downloadCSV(): void {
    let date = new Date();
    let d = "report_" + ((date.getDate() <= 9) ? ('0' + date.getDate()) : (date.getDate())) + '/' + ((date.getMonth() + 1) < 10 ? ('0' + (date.getMonth() + 1)) : ((date.getMonth() + 1))) + '/' + date.getFullYear();
    this.app_service.downloadFile(this.temp, d);
  }

  /**to change the language */
  public changeLang(language: string): void {
    this.translate.use(language);
  }

  /**Pagination */
  public paginate(event: any): void {
    if (this.source_filter_text || this.dest_filter_text || this.globalFilter) {
      let filterData = this.temp.filter(x => {
        if (this.source_filter_text && this.sourceFilter.length) {
          return x.s.toLowerCase().includes(this.source_filter_text.toLowerCase());
        }
        if (this.dest_filter_text && this.dest_filter_text.length) {
          return x.d.toLowerCase().includes(this.dest_filter_text.toLowerCase());
        }
        if (this.globalFilter && this.globalFilter.length) {
          return x.d.toLowerCase().includes(this.globalFilter.toLowerCase()) ||
            x.s.toLowerCase().includes(this.globalFilter.toLowerCase()) ||
            x.o.toLowerCase().includes(this.globalFilter.toLowerCase()) ||
            x.rule.toLowerCase().includes(this.globalFilter.toLowerCase()) ||
            x.hex.toLowerCase().includes(this.globalFilter.toLowerCase());
        }
      });
      this.totalRecords = filterData.length;
      this.rowsPerPage = event.rows;
      this.indexOfFirstRecord = event.first;
      let paginatedData = filterData.slice(this.indexOfFirstRecord, this.indexOfFirstRecord + this.rowsPerPage);
      this.rowsData = paginatedData;
    } else {
      this.totalRecords = this.temp.length;
      this.indexOfFirstRecord = event.first;
      this.rowsPerPage = event.rows;
      this.indexOfNewPage = event.page;
      let totalPages = event.pageCount;
      let paginatedData = this.temp.slice(this.indexOfFirstRecord, this.indexOfFirstRecord + this.rowsPerPage);
      this.rowsData = paginatedData;
    }

  }
  /** refresh the grid data */
  public reload(): void {
    this.hasData = true;
    this.loadingState = true;
    this.rowsData = [];
    this.app_service.getData().pipe(delay(1000)).subscribe((data: any) => {
      this.totalRecords = data.data.length;

      // return totalRecords;
      this.rowsData = data.data.map((row) => {
        let hex = ''
        row.data.map((a) => {
          hex += (a.offset) + (a.hex)
        });
        return {
          ts: row['ts'],
          s: row['s'],
          d: row['d'],
          o: row['o'],
          rule: row['rule'],
          hex: hex
        };
      });

      this.rowsData = this.temp.slice(0, this.rowsPerPage);
      this.hasData = false;
      this.loadingState = false;
    }, error => {
      this.loadingState = false;
      this.hasData = false;
    });

    localStorage.removeItem('source_filter_text');
    localStorage.removeItem('dest_filter_text');
    localStorage.removeItem('filter_text');
    localStorage.removeItem('start_date');
    localStorage.removeItem('end_date');
    this.source_filter_text = '';
    this.dest_filter_text = '';
    this.text = '';
    this.start_date = '';
    this.end_date = '';
  }

  /**Getting json data to display in prime DataTable */
  private getJsonData(): void {
    this.app_service.getData().subscribe((data: any) => {
      if (data) {
        this.totalRecords = data.data.length;
        this.temp = data.data;
        this.temp2 = data.data;
        this.rowsData = data.data.map((row) => {
          let hex = ''
          row.data.map((a) => {
            hex += (a.offset) + (a.hex)
          });
          return {
            ts: row['ts'],
            s: row['s'],
            d: row['d'],
            o: row['o'],
            rule: row['rule'],
            hex: hex
          };
        });
        this.temp = this.rowsData;
        this.rowsData = this.temp.slice(0, this.rowsPerPage);
        this.hasData = false;
        this.text = localStorage.getItem('filter_text');
        if (this.text) {
          this.globalFilter = this.text;
          this.filter(this.text);
        }
        this.source_filter_text = (localStorage.getItem('source_filter_text') === null ? '' : localStorage.getItem('source_filter_text'));
        if (this.source_filter_text) {
          this.sourceFilter();
        }
        this.dest_filter_text = (localStorage.getItem('dest_filter_text') === null) ? '' : localStorage.getItem('dest_filter_text');
        if (this.dest_filter_text) {
          this.destFilter();
        }
      }
    },
      (error) => {
        if (error.match(/404/g)) {
          this.errorMessage = '404 Not Found';
          this.haveError = true;
          this.hasData = false;
        } else {
          this.hasError = true;
          this.hasData = false;
        }
      }
    );
  }

}
